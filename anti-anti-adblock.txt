[Adblock Plus 2.0]
! Title: Testarossa - anti-anti-adblock
! Description: Based in anti-adblock-killer project (discontinued). Recommended to use together the following lists: https://easylist-downloads.adblockplus.org/antiadblockfilters.txt and https://gitcdn.xyz/repo/NanoAdblocker/NanoFilters/master/NanoMirror/NanoDefender.txt
! Expires: 9 days
! Homepage: https://gitlab.com/testa-rossa/adblock_filter_lists
! Source: https://gitlab.com/testa-rossa/adblock_filter_lists/-/raw/master/anti-anti-adblock.txt
###k2Uw7isHrMm5JXP1Vwdxc567ZKc1aZ4I
#@##banner_ad
#@##sponsored-ad
#@#.afs_ads
/AdblockDetector.js$script
/c-hive.js$domain=~authedmine.com
/c-hive.min.js$domain=~authedmine.com
/cn.wasm$domain=~authedmine.com
/coinhive.js$domain=~authedmine.com
/coinhive.min.js$domain=~authedmine.com
/cryptonight.wasm$domain=~authedmine.com
/detectadblock.
/wp-content/plugins/pause-adblocker/*
/wp-content/uploads/*$script,domain=altervista.org
@@/adframe.js$script
@@/advert.js$script,domain=~adultmult.tv
@@/advertisement*.js$script
@@/Advertisement.css$stylesheet,domain=openload.co|openload.io|openload.tv
@@/static/image/ad*.png$image,domain=adf.ly|j.gs|q.gs|ay.gy
@@||4j.com^$generichide
@@||9anime.to^$generichide
@@||ad.adview.pl/ad/$xmlhttprequest,domain=play.wtk.insys.pl
@@||ad.filmweb.pl/adbanner/reklamy/adx.js$script
@@||adf.ly^$generichide
@@||admin.brightcove.com^$object-subrequest,domain=tvn24.pl
@@||adserver.adtech.de/?adrawdata/$xmlhttprequest,domain=ruutu.fi
@@||adx.adform.net/adx$domain=vvvvid.it
@@||adx.adform.net/videoad/?$domain=vvvvid.it
@@||ailde.biz/a.sponsorads.js$domain=cbs.com,script
@@||ajax.googleapis.com/ajax/libs/jquery/$script,domain=gelbooru.com
@@||amazonaws.com/socketloop/show_ads_epmads.js$script,domain=socketloop.com
@@||analytics.edgesuite.net^$domain=cbs.com,object-subrequest
@@||anonymousemail.me^$generichide
@@||authedmine.com^$domain=coinhive.com|filecrypt.cc
@@||ay.gy^$generichide
@@||bdfrm.bidvertiser.com/BidVertiser.dbm$subdocument,domain=exrapidleech.info
@@||bild.de^$elemhide
@@||cdn.jsdelivr.net/jwplayer/5.10/jwplayer.js$script
@@||clik.pw^$generichide
@@||cloudvideoplatform.com/advert.jpg$image,domain=nana10.co.il
@@||coolkora.com^$elemhide
@@||cpubenchmark.net/js/ads.js$script
@@||crackle.com/vendor/AdManager.js$script
@@||datalog.co.uk/js/ads.js$script
@@||dbzog.de^$elemhide
@@||des.smartclip.net/ads?$domain=vvvvid.it
@@||designtaxi.com/js/ads.js$script
@@||di.se^$elemhide
@@||folha.uol.com.br/*/publicidade.ads.js$script
@@||fptplay.net/js/ads.js$script
@@||fwmrm.net^$xmlhttprequest,domain=q2.be|vtm.be|medialaancdn.be|medialaan.be
@@||fwmrm.net^$xmlhttprequest,domain=vtm.be
@@||github.com/reek/anti-adblock-killer/
@@||githubusercontent.com/reek/anti-adblock-killer/
@@||googleads.g.doubleclick.net/pagead/ads$subdocument,third-party,domain=~afreesms.com
@@||hackintosh.computer/*ad$script
@@||hackintosh.computer^$generichide
@@||hackintosh.zone^*/ads.js$script
@@||heavenly-blue.net^$elemhide
@@||hindustantimes.com^/ads.js$script
@@||hubturkey.net/js/ads.js$script
@@||hulu.com^$generichide
@@||imasdk.googleapis.com$domain=vvvvid.it
@@||imasdk.googleapis.com/js/sdkloader/ima3.js$script,domain=news.voicetv.co.th
@@||indiatimes.com/ads.cms$script
@@||indiatimes.com/toi_js_ads/ads/$script
@@||j.gs^$generichide
@@||jsuol.com.br/c/detectadblock/$script,domain=uol.com.br
@@||jwpsrv.com/library/5V3tOP97EeK2SxIxOUCPzg.js$script
@@||kiplinger.com^$elemhide
@@||kozaczek.pl/js/ads.js$script,domain=kozaczek.pl
@@||kwejk.pl^$elemhide
@@||lequipe.fr/*/js/ads-v*.js$script
@@||lggautotrasporti.esy.es^$elemhide
@@||linkneverdie.com^$generichide
@@||lmss.vn^$generichide
@@||lolalytics.com^$generichide
@@||maisgasolina.com^$elemhide
@@||manifest.auditude.com^$xmlhttprequest,domain=pga.com
@@||mmm.dk^$elemhide
@@||moatads.com/*/moatwrapper.js$script,domain=hulu.com|viz.com
@@||ncaa.com^$generichide
@@||oloadcdn.net^$domain=pornvibe.org
@@||openload.co/embed^$third-party,domain=pornvibe.org
@@||openload.co^$elemhide
@@||openload.io^$elemhide
@@||openload.tv^$elemhide
@@||pagead2.googlesyndication.com/pagead/$script,domain=yellowbridge.com
@@||pagead2.googlesyndication.com/pagead/js/$script,domain=apkmirror.com
@@||pagead2.googlesyndication.com/pagead/js/adsbygoogle.js$script,domain=al.ly
@@||pagead2.googlesyndication.com/pagead/osd.js$script,domain=socketloop.com
@@||pagead2.googlesyndication.com^$script,domain=hackintosh.computer
@@||pga.com^$generichide
@@||play.wtk.insys.pl/Scripts/InsysPlayer.v0.1.0/adTest.png$image
@@||politiken.dk/*ad$script
@@||pubads.g.doubleclick.net^$object-subrequest,domain=cbsnews.com
@@||q.gs^$generichide
@@||receive-a-sms.com/showads.js$xmlhttprequest
@@||s1-adfly.com/show.php$subdocument,domain=adf.ly|j.gs|q.gs|ay.gy
@@||salon.com/combo/js$script
@@||sascdn.com^$script,domain=bild.de
@@||satoshiquiz.com/ad.js$script
@@||satoshiquiz.com/ads.js$script
@@||secure-dcr.imrworldwide.com/novms/js/$script,domain=crackle.com
@@||skiplimite.tv^$elemhide
@@||smartadserver.com/config.js$script,domain=lequipe.fr
@@||smartadserver.com/diff/js/smart.js$script,domain=lequipe.fr
@@||smartadserver.com^$script,domain=bild.de
@@||spaste.com/ads/$subdocument
@@||spaste.com^$generichide
@@||squid.gazeta.pl/info/squid/fprint.js$script,domain=wyborcza.pl|gazeta.pl
@@||squid.gazeta.pl/sqpwgl/pwfpl$xmlhttprequest,domain=wyborcza.pl|gazeta.pl
@@||squid.gazeta.pl/sqpwgl/pwif$subdocument,domain=wyborcza.pl|gazeta.pl
@@||static.adf.ly/static/image/ad*.png$image,third-party
@@||static.vvvvid.it/img/ad$domain=vvvvid.it
@@||straight-world.de/js/ads.js$script
@@||t2e.pl/www/js/ads.js$script
@@||tidaltv.com/ILogger.aspx$object,domain=itv.com
@@||tv3sport.dk^$elemhide
@@||uol.com.br^$generichide
@@||urbeez.com^$elemhide
@@||vidoza.net/js/ads.js$script
@@||vidoza.net/js/pop.js$script
@@||vjs.zencdn.net/4.12/video.js$script
@@||vstrim.pl/js/ads.js$script,domain=vstrim.pl
@@||vvvvid.it/img/ad/advertisement.xml$xmlhttprequest
@@||wp.pl^$script,image
@@||yellowbridge.com^$elemhide
@@||ynet.co.il/common/javascript/ads.js$script,~third-party
@@|$image,domain=socketloop.com
@@|$script,domain=sport1.de
@@|$xmlhttprequest,image,domain=cbsnews.com
@@plugins/blockalyzer-adblock-counter/img/ads/$image
@||crunchyroll.com/*/ads_enabled_flag*.js$script
||$third-party,script,domain=pornvibe.org
||35.184.169.188^$popup
||ads-v-darwin.hulustream.com^
||assets.gelbooru.com/r18/
||assets.gelbooru.com/r19/
||authedmine.com^$third-party
||cdn.adf.ly/js/display.js$script,third-party
||cdn.adf.ly/js/entry.js$script,third-party
||cdn.adf.ly/js/link-converter.js$script,third-party
||cdn.ay.gy/js/display.js$script,third-party
||cdn.ay.gy/js/entry.js$script,third-party
||cdn.ay.gy/js/link-converter.js$script,third-party
||cdn.j.gs/js/display.js$script,third-party
||cdn.j.gs/js/entry.js$script,third-party
||cdn.j.gs/js/link-converter.js$script,third-party
||cdn.q.gs/js/display.js$script,third-party
||cdn.q.gs/js/entry.js$script,third-party
||cdn.q.gs/js/link-converter.js$script,third-party
||cqfnvznw.info^
||d.0emm.com^$third-party
||d.0emn.com^$third-party
||d.0enm.com^$third-party
||d.0fmm.com^$third-party
||d.0mme.com^$third-party
||d.1emn.com^$third-party
||d.1enm.com^$third-party
||d.2enm.com^$third-party
||d.3enm.com^$third-party
||d.4666.ch^$third-party
||d.8d1f.com^$third-party
||d.9emn.com^$third-party
||d.e0mn.com^$third-party
||d.em0n.com^$third-party
||d.emn0.com^$third-party
||d.m80fg.com^$third-party
||d.mdf91.com^$third-party
||d.mn0e.com^$third-party
||d.mpk01.com^$third-party
||d.ndf81.com^$third-party
||d.nfd81.com^$third-party
||d.nn0e.com^$third-party
||d.rpts.org^$third-party
||doubleclick.net^$domain=aetv.com|history.com|mylifetime.com|receive-a-sms.com
||googlesyndication.com^$domain=short.am|clubedohardware.com.br|receive-a-sms.com
||hammerhearing.com^$third-party
||idalia.assariaburdine.com/happiness.js$script,domain=lequipe.fr
||indiatimes.com/ads_native_js^$script
||indiatimes.com/detector.cms$script
||indiatimes.com/detector/$script
||indiatimes.in/*/detector.js$script
||jyhfuqoh.info^
||kissasian.com/*.aspx
||ladnydom.pl/aliasy/ladnydom/adblock_prompt.htm
||ladnydom.pl/pub/ips/
||lequipe.fr/v6/js/popinVideo.js$script
||lib.onet.pl^*/adp/adp.js$script
||optimizely.com^$third-party
||perceivequarter.com^$third-party
||reek.github.io/anti-adblock-killer/k2Uw7isHrMm5JXP1Vwdxc567ZKc1aZ4I.js|$script
||sixscissors.com^$third-party
||spike.itv.com/itv/tserver/size=*/viewid=
||taboola.com^$domain=spaste.com
||thedoujin.com/bload.php
||tom.itv.com/itv/tserver/size=
||uol.com.br/*/detectadblock/$script
||w.0emm.com^$third-party
||w.0emn.com^$third-party
||w.0enm.com^$third-party
||w.0fmm.com^$third-party
||w.0mme.com^$third-party
||w.1emn.com^$third-party
||w.1enm.com^$third-party
||w.2enm.com^$third-party
||w.3enm.com^$third-party
||w.4666.ch^$third-party
||w.8d1f.com^$third-party
||w.9emn.com^$third-party
||w.e0mn.com^$third-party
||w.em0n.com^$third-party
||w.emn0.com^$third-party
||w.m80fg.com^$third-party
||w.mdf91.com^$third-party
||w.mn0e.com^$third-party
||w.mpk01.com^$third-party
||w.ndf81.com^$third-party
||w.nfd81.com^$third-party
||w.nn0e.com^$third-party
||w.rpts.org^$third-party
|$third-party,script,domain=hothardware.com
|$third-party,script,domain=photobucket.com
|$third-party,script,domain=streamwoop.tv
|$third-party,script,domain=uptobox.com|uptostream.com
|$third-party,xmlhttprequest,domain=streamwoop.tv
123link-8989.kxcdn.com/1/2/jquery.adi.js
4j.com###game_bottom_ad
4j.com###game_top_ad
9anime.to,mycloud.to###jwa
apkmirror.com#@#.adsbygoogle
asset.pagefair.com/measure.min.js
asset.pagefair.net/ads.min.js
avgle.com###aoverlay
darmowe-pornosy.pl###porno_accept
fox.com.tr#@#.pub_300x250
fox.com.tr#@#.pub_300x250m
fox.com.tr#@#.pub_728x90
fox.com.tr#@#.text_ad
fox.com.tr#@#.text_ads
fox.com.tr#@#.text-ad
fox.com.tr#@#.textAd
fox.com.tr#@#.text-ad-links
fox.com.tr#@#.text-ads
gej-porno.pl,animezone.pl,egy.best,newpct.com##a[href^="//"]
gelbooru.com##.noticeError
gelbooru.com#@##paginator
gelbooru.com#@#[style*="height:"][width]
gelbooru.com#@#[style*="width:"][height]
gelbooru.com#@#center > a[href]
hackintosh.computer###googleads
hackintosh.computer##.adscenter
hulu.com###banner-ad-container
hulu.com##.ad-choices
imagebam.com###hideifpossible
indiatimes.com#@#div[id^="div-gpt-ad"]
mms.kotaku.com$script
mms.kotaku.com$xmlhttprequest
nakednepaligirl.com##.rmedia
perveden.com###topBoxContainer+div
pornhub.com,tube8.fr,tube8.es,tube8.com###abAlert
pornhub.com,tube8.fr,tube8.es,tube8.com###adBlockAlertWrap
pornvibe.org##.special-message-content
rule34hentai.net###babasbmsgx
smashboards.com###noticeMain
smashboards.com##.premiumContain
socialblade.com#@##bottomAd
socketloop.com##.adtester-container
socketloop.com#@##banner_ads
socom.yokohama##DIV[class="drIcxojzIxKe-bg"]
socom.yokohama##DIV#tlhrJGlzhzjO[class="drIcxojzIxKe"]
softpedia.com#@#.ad
telegraph.co.uk##.adblocker-message
androidiani.com##DIV#cboxOverlay
androidiani.com##DIV#colorbox
