Recommended filter lists for uBlock Origin (or others adblocks):

First activate all the original filter lists from uBlock and AdGuard, then add the desired lists from the list below. 

# MY FILTER LISTS FOR ADBLOCK

**Anti-anti-adblock:** https://gitlab.com/testa-rossa/adblock_filter_lists/-/raw/master/anti-anti-adblock.txt

**Anti-paywall:** https://gitlab.com/testa-rossa/adblock_filter_lists/-/raw/master/anti-paywall.txt

**Blacklist IPs:** https://gitlab.com/testa-rossa/adblock_filter_lists/-/raw/master/blacklistips.txt

**Fanboy's Brasil:** https://gitlab.com/testa-rossa/adblock_filter_lists/-/raw/master/fanboy_Brasil.txt

**Generic filters:** https://gitlab.com/testa-rossa/adblock_filter_lists/-/raw/master/genericfilters.txt

**Malware domains:** https://gitlab.com/testa-rossa/adblock_filter_lists/-/raw/master/malwaredomains.txt

**Whitelist:** https://gitlab.com/testa-rossa/adblock_filter_lists/-/raw/master/whitelist.txt

**Clone of Easylist Adblock Warning Removal List (to add to uBlock Origin):** https://gitlab.com/testa-rossa/adblock_filter_lists/-/raw/master/antiadblockfilters.txt

**Clone of Brave-Disconnect (discontinued):** https://gitlab.com/testa-rossa/adblock_filter_lists/-/raw/master/brave-disconnect.txt

**Clone of ABP filters anti circumvention (to add to uBlock Origin):** https://gitlab.com/testa-rossa/adblock_filter_lists/-/raw/master/abp-filters-anti-cv.txt

# OTHER RECOMMENDED FILTER LISTS

## ANTI-ANNOUNCEMENT FILTERS

**uBlock (RECOMMENDED):** https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/filters.txt

**AdGuard:** https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_2_English/filter.txt

**AdGuard - Mobile Ads:** https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_11_Mobile/filter.txt

**Easylist:** https://easylist.to/easylist/easylist.txt
**Link 2:** https://secure.fanboy.co.nz/easylist.txt
**Link 3:** https://easylist-downloads.adblockplus.org/easylist.txt

**Nano (RECOMMENDED):** https://gitcdn.xyz/repo/NanoAdblocker/NanoFilters/master/NanoFilters/NanoBase.txt

**Pringent-Ads:** https://v.firebog.net/hosts/Prigent-Ads.txt

## ANTI -TRACKING FILTERS

**uBlock (RECOMMENDED):** https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/privacy.txt

**AdGuard:** https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_3_Spyware/filter.txt

**Easyprivacy):** https://easylist.to/easylist/easyprivacy.txt
**Link 2:** https://secure.fanboy.co.nz/easyprivacy.txt
**Link 3:** https://easylist-downloads.adblockplus.org/easyprivacy.txt

**Fanboy:** https://secure.fanboy.co.nz/enhancedstats.txt

**quidsup.net (RECOMMENDED):** https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-blocklist.txt

**frogeye.fr (RECOMMENDED):** https://hostfiles.frogeye.fr/firstparty-trackers-hosts.txt

## ANTI-MALWARE FILTERS

**uBlock (RECOMMENDED):** https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/badware.txt

**Urlhaus:** https://curben.gitlab.io/malware-filter/urlhaus-filter-online.txt
**Link 2:** https://gitlab.com/curben/urlhaus-filter/-/raw/master/urlhaus-filter-online.txt

**Nano:** https://gitcdn.xyz/repo/NanoMeow/MDLMirror/master/hosts.txt

**Pringent-Malware:** https://v.firebog.net/hosts/Prigent-Malware.txt

**Shalla-mal:** https://v.firebog.net/hosts/Shalla-mal.txt

**Disconnect Simple Malvertising:** https://s3.amazonaws.com/lists.disconnect.me/simple_malvertising.txt

## ANTI-SCAM FILTERS

**Spam404 (RECOMMENDED):** https://raw.githubusercontent.com/Spam404/lists/master/adblock-list.txt

**OpenPhish (RECOMMENDED):** https://openphish.com/feed.txt

**Phishing Army (RECOMMENDED):** https://phishing.army/download/phishing_army_blocklist_extended.txt

**emergingthreats.net:** https://rules.emergingthreats.net/fwrules/emerging-Block-IPs.txt

**firehol.org level 1 (RECOMMENDED):** https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/firehol_level1.netset

**firehol.org level 2 (RECOMMENDED):** https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/firehol_level2.netset

**Joewein:** https://joewein.net/dl/bl/dom-bl-base.txt

## ANTI-ANNOYANCES FILTERS

**uBlock (RECOMMENDED):** https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/annoyances.txt

**AdGuard:** https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_14_Annoyances/filter.txt

**Fanboy (includes Fanboy-Social, Fanboy anti-cookie, Fanboy's Notifications):** https://easylist-downloads.adblockplus.org/fanboy-annoyance.txt
**Link 2:** https://secure.fanboy.co.nz/fanboy-annoyance.txt

**Web Annoyances Ultralist (RECOMMENDED):** https://raw.githubusercontent.com/yourduskquibbles/webannoyances/master/ultralist.txt

**Fanboy's Notifications Blocking List:** https://easylist-downloads.adblockplus.org/fanboy-notifications.txt

## ANTI COOKIE WARNINGS

**Fanboy:** https://secure.fanboy.co.nz/fanboy-cookiemonster.txt

**I don't care about cookies (RECOMMENDED):** https://www.i-dont-care-about-cookies.eu/abp/

**Probake (obsolete):** https://raw.githubusercontent.com/liamja/Prebake/master/obtrusive.txt

## ANTI SOCIAL MEDIA FILTERS

**AdGuard:** https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_4_Social/filter.txt

**Fanboy:** https://easylist.to/easylist/fanboy-social.txt
**Link 2:** https://easylist-downloads.adblockplus.org/fanboy-social.txt

**Fanboy antifacebook:** https://secure.fanboy.co.nz/fanboy-antifacebook.txt

## FILTROS PARA PORTUGUÊS/BRASIL

**AdGuard:** https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_9_Spanish/filter.txt

**Easylist:** https://easylist-downloads.adblockplus.org/easylistportuguese.txt

**EasylistBrasil:** https://raw.githubusercontent.com/easylistbrasil/easylistbrasil/filtro/easylistbrasil.txt

**Fanboy (obsolete):** https://secure.fanboy.co.nz/fanboy-espanol.txt

## ALL-IN-ONE FILTERS LIST

**Fanboy Complete List (includes Easylist, Easyprivacy, Enhanced Trackers List):** https://secure.fanboy.co.nz/r/fanboy-complete.txt

**Fanboy Ultimate List (RECOMMENDED - includes Easylist, Easyprivacy, Enhanced Trackers List and Annoyances List):** https://secure.fanboy.co.nz/r/fanboy-ultimate.txt

**AdGuard DNS (RECOMMENDED - includes social media, privacy/spyware, mobile, EasyList and EasyPrivacy):** https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_15_DnsFilter/filter.txt
**Link2:** https://adguardteam.github.io/AdGuardSDNSFilter/Filters/filter.txt

**StevenBlack (RECOMMENDED - includes AdAway, hostVN, MVPS hosts, Dan Pollock, URLHaus, Peter Lowe's, and more):** https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-social/hosts

**Adaway:** https://adaway.org/hosts.txt

**Peter Lowe's servers host:** https://pgl.yoyo.org/adservers/serverlist.php?hostformat=adblockplus&showintro=1&mimetype=plaintext

**Dan Pollock’s hosts:** https://someonewhocares.org/hosts/hosts

**MVPS hosts:** https://winhelp2002.mvps.org/hosts.txt

**CPBL Filters (ads, trackers/telemetry, malware and scams):** https://raw.githubusercontent.com/bongochong/CombinedPrivacyBlockLists/master/cpbl-abp-list.txt

## ANTI-ANTI-ADBLOCK

**Reek's Anti-Adblock Killer (obsolete):** https://raw.github.com/reek/anti-adblock-killer/master/anti-adblock-killer-filters.txt

**AAK-Cont Filter (obsolete):** https://gitlab.com/xuhaiyang1234/AAK-Cont/-/raw/master/FINAL_BUILD/aak-cont-list-ubo.txt

**Nano Defender (RECOMMENDED):** https://gitcdn.xyz/repo/NanoAdblocker/NanoFilters/master/NanoMirror/NanoDefender.txt

**Anti-adblock Warning (RECOMMENDED):** https://easylist-downloads.adblockplus.org/antiadblockfilters.txt

## WHITELIST

**uBlock (RECOMMENDED):** https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/unbreak.txt

**Brave:** https://raw.githubusercontent.com/brave/adblock-lists/master/brave-unbreak.txt

## OTHER FILTERS

**uBlock resource abuse (RECOMMENDED):** https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/resource-abuse.txt

**NoCoin:** https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/nocoin.txt

**BarbBlock (sites use DMCA to removal blocking lists):** https://paulgb.github.io/BarbBlock/blacklists/adblock-plus.txt

**Anti-circumvention ads:** https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt
**Link 2:** https://easylist-msie.adblockplus.org/abp-filters-anti-cv.txt

**Thirdparty Fonts Filters:** https://secure.fanboy.co.nz/fanboy-antifonts.txt

**uBlock Plus:** https://raw.githubusercontent.com/IDKwhattoputhere/uBlock-Filters-Plus/master/uBlock-Filters-Plus.txt

**EXTRA:** Open uBlock Origin dashboard, select “Settings” tab, check “I am an advanced user”, click the gears icon that shows up, replace “unset” after “userResourcesLocation” by: https://gitcdn.xyz/repo/NanoAdblocker/NanoFilters/master/NanoFilters/NanoResources.txt

### ORIGINAL PAGE OF FILTERS:

https://github.com/uBlockOrigin/uAssets/tree/master/filters

https://kb.adguard.com/en/general/adguard-ad-filters

https://github.com/AdguardTeam/FiltersRegistry/tree/master/filters

https://easylist.to/

https://adblockplus.org/subscriptions

https://fanboy.co.nz/index.html

https://www.fanboy.co.nz/regional.html

https://firebog.net/

https://github.com/NanoAdblocker/NanoFilters/blob/master/GitCDN.MD

https://jspenguin2017.github.io/uBlockProtector/#extra-installation-steps-for-ublock-origin

https://filterlists.com/

https://gitlab.com/testa-rossa/adblock_filter_lists
